﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionDamage : MonoBehaviour {
    public string playerTag = "Player";
    internal Transform playerObject;

    //public GameObject explosion;

    public int damage = 10;
	void Start () {
        playerObject = GameObject.FindGameObjectWithTag(playerTag).transform;
    }
	
	// Update is called once per frame
	void Update () {
       
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            if (!playerObject.GetComponent<PlayerHealth>().invincible)
            {
                DamagePlayer();
            }
            if (playerObject.GetComponent<PlayerHealth>().invincible)
            {

            }
        }
    }

    void DamagePlayer()
    {
        playerObject.SendMessageUpwards("TakeDamage", damage);
        
        //explosion.SetActive(false);
    }
}
