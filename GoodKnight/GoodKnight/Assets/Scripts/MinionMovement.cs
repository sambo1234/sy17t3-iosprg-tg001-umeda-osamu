﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionMovement : MonoBehaviour {
    public string playerTag = "Player";
    internal Transform playerObject;

    [SerializeField]
    private float moveSpeed = 2f;

    Rigidbody2D rb;

    Vector3 pointA;
    Vector3 pointB;
    Vector3 localScale;

    float dirX;

    bool facingRight = false;
    // Use this for initialization
    void Start()
    {
        pointA = new Vector3(transform.position.x, transform.position.y, 0);
        pointB = new Vector3(transform.position.x + 6, transform.position.y, 0);
        playerObject = GameObject.FindGameObjectWithTag(playerTag).transform;
        localScale = transform.localScale;
        rb = GetComponent<Rigidbody2D>();
        dirX = -1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        //if(transform.position.x < (transform.position.x - 1f))
        //{
        //    dirX = 1f;
        //}
        //if (transform.position.x > (transform.position.x + 1f))
        //{
        //    dirX = -1f;
        //}
        float time = Mathf.PingPong(Time.time * moveSpeed, 1);
        transform.position = Vector3.Lerp(pointA, pointB, time);
    }

    void FixedUpdate()
    {
        rb.velocity = new Vector2(dirX * moveSpeed, rb.velocity.y);
    }

    void LateUpdate()
    {
        CheckWhereToFace();
    }

    void CheckWhereToFace()
    {
        if (dirX > 0)
            facingRight = true;

        else if (dirX < 0)
            facingRight = false;


        if (((facingRight) && (localScale.x < 0)) || ((!facingRight) && (localScale.x > 0)))
            localScale.x *= -1;

        transform.localScale = localScale;

    }
}
