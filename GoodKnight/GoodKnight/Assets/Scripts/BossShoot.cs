﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossShoot : MonoBehaviour {
	public GameObject projectile;
	public float shootTime;
	public int chanceShoot;
	public Transform shootFrom;

	float nextShot;

	void Start()
	{
		nextShot = 0f;
	}

	void OnTriggerStay2D(Collider2D col)
	{
		if (col.tag == "Player" && nextShot < Time.time) {
			nextShot = Time.time + shootTime;
			if (Random.Range (0, 10) >= chanceShoot) {
                GameObject bullet = ObjectPooler.sharedInstance.GetPooledObject();
                if (bullet != null)
                {
                    bullet.transform.position = shootFrom.transform.position;
                    bullet.transform.rotation = shootFrom.transform.rotation;
                    bullet.SetActive(true);
                }
            }
		}
	}
}
