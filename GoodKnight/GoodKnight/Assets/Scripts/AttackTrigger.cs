﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AttackTrigger : MonoBehaviour
{
    private AsyncOperation sceneAsync;

    public GameObject player;
    public GameObject playerCanvas;
    public GameObject BombDestroyerTrigger;

    public int skillPoints;

    public Button addDamageBtn;
    public Button increaseSwordSizeBtn;
    public Text skillPointsCount;

    public Collider2D attackTrigger;

    public int dmg = 20;
    public int dmgIncrease;
    public float increaseSize;

    public bool levelIsFinished;

    public GameObject canvas;

    private BoxCollider2D SwordSize;
    // Use this for initialization
    void Start()
    {
        Button btn = addDamageBtn.GetComponent<Button>();
        Button btn2 = increaseSwordSizeBtn.GetComponent<Button>();
        SwordSize = GetComponent<BoxCollider2D>();
        btn.onClick.AddListener(AddDamage);
        btn2.onClick.AddListener(IncreaseSwordSize);
        skillPointsCount.text = " " + skillPoints.ToString();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.isTrigger != true && col.CompareTag("Enemy"))
        {
            col.SendMessageUpwards("Damage", dmg);
        }
        if (col.isTrigger != true && col.CompareTag("Boss"))
        {
            col.SendMessageUpwards("Damage", dmg);
        }
    }

    public void IncreaseSwordSize()
    {
        skillPoints -= 1;
        if (skillPoints > -1)
        {
            SwordSize.size += new Vector2(increaseSize, 0);
        }
        else if (skillPoints <= 0)
        {
            skillPoints = 0;
            Debug.Log("Insufficient Skill points");
            StartCoroutine(ChangeScene());
            levelIsFinished = true;
            canvas.SetActive(false);
        }
        skillPointsCount.text = " " + skillPoints.ToString();
    }

    public void AddDamage()
    {
        skillPoints-= 1;
        if (skillPoints > -1)
        {
            dmg += dmgIncrease;
        }
        else if(skillPoints <= 0)
        {
            skillPoints = 0;
            Debug.Log("Insufficient Skill points");
            StartCoroutine(ChangeScene());
            levelIsFinished = true;
            canvas.SetActive(false);
        }
        skillPointsCount.text = " " + skillPoints.ToString();
    }

    IEnumerator ChangeScene()
    {
        SceneManager.LoadScene(2, LoadSceneMode.Additive);
        Scene nextScene = SceneManager.GetSceneAt(SceneManager.sceneCount - 1);
        SceneManager.MoveGameObjectToScene(player, nextScene);
        SceneManager.MoveGameObjectToScene(playerCanvas, nextScene);
        SceneManager.MoveGameObjectToScene(BombDestroyerTrigger, nextScene);
        yield return null;
        SceneManager.UnloadSceneAsync(2 - 1);
    }
}
