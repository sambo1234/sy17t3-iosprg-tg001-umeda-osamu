﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCheck : MonoBehaviour {

    public void ReceiveTrigger(ref Collider2D col)
    {
        Debug.Log("Successfully collided with:" );
        OnTriggerEnter2D(col);
    }

    public void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "Player")
        {
            Debug.Log("Successfully collided!");
        }
    }
}
