﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour {
    private Rigidbody2D rb;
    private Animator anim;

    public float moveForce;
    public float maxSpeed;

    private bool facingRight = true;

    [SerializeField]
    private float moveSpeed;

    [SerializeField]
    private float dashForce;

    [SerializeField]
    private Transform[] groundPoints;

    [SerializeField]
    private float groundRadius;

    [SerializeField]
    private LayerMask whatIsGround;

    public bool isGrounded;

    [SerializeField]
    private float jumpForce;

    private bool jump;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = gameObject.GetComponent<Animator>();
    }

    void Update()
    {
       
    }
    void FixedUpdate()
    {
       
        float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");

        if (CrossPlatformInputManager.GetButtonDown("Jump"))
        {
            jump = true;
        }

        isGrounded = IsGrounded();

        HandleMovement(horizontal);

        Flip(horizontal);

        HandleLayers();

        jump = false;
    }

    private void HandleMovement(float horizontal)
    {

        //if (horizontal * rb.velocity.x < maxSpeed)
        //    rb.AddForce(Vector2.right * horizontal * moveForce);

        //if (Mathf.Abs(rb.velocity.x) > maxSpeed)
        //    rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * maxSpeed, rb.velocity.y);

        if(rb.velocity.y < 0)
        {
            anim.SetBool("Land", true);
        }
 
        if (horizontal * rb.velocity.x < maxSpeed)
            transform.Translate(Vector2.right * horizontal * maxSpeed);

        anim.SetFloat("Speed", Mathf.Abs(horizontal));

        if (isGrounded && jump)
        {
            isGrounded = false;
            rb.AddForce(new Vector2(0, jumpForce));
            anim.SetTrigger("Jump");
        }
    }

    private void HandleInput()
    {
        if (CrossPlatformInputManager.GetButtonDown("Jump"))
        {
            jump = true;
        }
    }

    private void Flip(float horizontal)
    {
        if(horizontal > 0.1f && !facingRight || horizontal < -0.1f && facingRight)
        {
            facingRight = !facingRight;

            Vector3 theScale = transform.localScale;

            theScale.x *= -1;

            transform.localScale = theScale;
        }
    }


    private bool IsGrounded()
    {
        if(rb.velocity.y <= 0)
        {
            foreach(Transform point in groundPoints)
            {
                Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position, groundRadius, whatIsGround);

                for(int i = 0; i < colliders.Length; i++)
                {
                    if(colliders[i].gameObject != gameObject)
                    {
                        anim.ResetTrigger("Jump");
                        anim.SetBool("Land", false);
                        return true;

                    }
                }
            }
        }
        return false;
    }

    private void HandleLayers()
    {
        if(!isGrounded)
        {
            anim.SetLayerWeight(1, 1);
        }
        else
        {
            anim.SetLayerWeight(1, 0);
        }
    }
}
