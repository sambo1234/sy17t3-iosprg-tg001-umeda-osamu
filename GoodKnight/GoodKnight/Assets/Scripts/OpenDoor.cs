﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour {
    public string playerTag = "Player";
    internal GameObject playerObject;
    public GameObject boss2Health;

    //public Getkey keyObtained;
    // Use this for initialization
    void Start () {
        playerObject = GameObject.FindGameObjectWithTag(playerTag).gameObject;
    }
	
	// Update is called once per frame
	void Update () {
		if(playerObject.GetComponent<Getkey>().keyObtained)
        {
            boss2Health.SetActive(true);
            Destroy(gameObject);
        }
	}
}
