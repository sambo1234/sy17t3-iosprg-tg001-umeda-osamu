﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {
    [SerializeField]
    private float fillAmount;

    [SerializeField]
    private Image content;

    public float health;
    public float maxHealth;

    public bool invincible;

    public float spriteBlinkingTimer = 0.0f;
    public float spriteBlinkingMiniDuration = 0.1f;
    public float spriteBlinkingTotalTimer = 0.0f;
    public float spriteBlinkingTotalDuration = 1.0f;
    public bool startBlinking = false;

    // Use this for initialization
    void Start () {
        invincible = false;
        health = maxHealth;
	}
	
	// Update is called once per frame
	void Update () {
        
        if (startBlinking == true)
        {
           
            SpriteBlinkingEffect();
        }
        HandleBar();
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        startBlinking = true;
        //StartCoroutine(Invincibility());
        if (health <= 0)
        {
            Destroy(gameObject);
        }
        
    }

    public void Heal(int value)
    {
        health += value;
        if (health > maxHealth)
        {
            health = maxHealth;
        }
    }

    private void HandleBar()
    {
        content.fillAmount = Map(health,maxHealth,fillAmount);
    }

    private float Map(float value, float inMax, float outMax)
    {
        return (value) * (outMax) / (inMax);
    }

    IEnumerator Invincibility()
    {
        invincible = true;
        yield return new WaitForSeconds(2);
        invincible = false;
    }

    private void SpriteBlinkingEffect()
    {
        StartCoroutine(Invincibility());
        spriteBlinkingTotalTimer += Time.deltaTime;
        if (spriteBlinkingTotalTimer >= spriteBlinkingTotalDuration)
        {
            startBlinking = false;

            spriteBlinkingTotalTimer = 0.0f;
            this.gameObject.GetComponent<SpriteRenderer>().enabled = true;   // according to 
                                                                             //your sprite
            return;
        }

        spriteBlinkingTimer += Time.deltaTime;
        if (spriteBlinkingTimer >= spriteBlinkingMiniDuration)
        {
            
            spriteBlinkingTimer = 0.0f;
            if (this.gameObject.GetComponent<SpriteRenderer>().enabled == true)
            {
                this.gameObject.GetComponent<SpriteRenderer>().enabled = false;  //make changes
            }
            else
            {
                this.gameObject.GetComponent<SpriteRenderer>().enabled = true;   //make changes
            }
        }
    }
}
