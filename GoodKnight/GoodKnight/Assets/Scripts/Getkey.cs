﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Getkey : MonoBehaviour {
    public bool keyObtained = false;

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Key")
        {
            keyObtained = true;
            collision.gameObject.SetActive(false);
           
        }
    }
}
