﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss1Activate : MonoBehaviour {
    public GameObject boss1;
    public GameObject boss1Wall;
    public GameObject boss1HealthBar;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            boss1.SetActive(true);
            boss1Wall.SetActive(true);
            boss1HealthBar.SetActive(true);
        }
    }
}
