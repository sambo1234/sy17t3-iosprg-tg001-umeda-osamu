﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapMovement : MonoBehaviour {
    public float moveSpeed;
    public float maxDistance;
    Vector3 pointA;
    Vector3 pointB;
    // Use this for initialization
    void Start()
    {
        pointA = new Vector3(transform.position.x, transform.position.y, 0);
        pointB = new Vector3(transform.position.x + maxDistance, transform.position.y, 0);
    }

    // Update is called once per frame
    void Update()
    {
        float time = Mathf.PingPong(Time.time * moveSpeed, 1);
        transform.position = Vector3.Lerp(pointA, pointB, time);
    }
}
