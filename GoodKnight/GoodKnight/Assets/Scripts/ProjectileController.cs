﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour {
	public float projectileSpeedHigh;
	public float projectileSpeedLow;
	public float projectileAngle;
	public float projectileTorqueAngle;

	Rigidbody2D projectileRB;

	// Use this for initialization
	void Start () {
		projectileRB = GetComponent<Rigidbody2D> ();
		projectileRB.AddForce (new Vector2 (Random.Range (-projectileAngle, projectileAngle), Random.Range (projectileSpeedLow, projectileSpeedHigh)),ForceMode2D.Impulse);
		projectileRB.AddTorque((Random.Range(-projectileTorqueAngle,projectileTorqueAngle)));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
