﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapAttack : MonoBehaviour {
    public string playerTag = "Player";
    internal Transform playerObject;

    private bool attacking = false;

    public float attackRange;
    //public int damage;
    public Collider2D attackTrigger;
    //private float LastAttackTime;
    //public float attackDelay;

    private float attackTimer = 0;
    private float attackCD = 3.0f;

    // Use this for initialization
    void Start()
    {
        playerObject = GameObject.FindGameObjectWithTag(playerTag).transform;
    }

    void Awake()
    {
        attackTrigger.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        Attack();
    }

    void Attack()
    {
        //check distance between boss and player
        float distanceToPlayer = Vector3.Distance(transform.position, playerObject.position);
        if (distanceToPlayer < attackRange && !attacking)
        {
            attacking = true;
            attackTimer = attackCD;
            attackTrigger.enabled = true;
        }
        if (attacking)
        {
            if (attackTimer > 0)
            {
                attackTimer -= Time.deltaTime;
            }
            else
            {
                attacking = false;
                attackTrigger.enabled = false;
            }
        }
    }
}
