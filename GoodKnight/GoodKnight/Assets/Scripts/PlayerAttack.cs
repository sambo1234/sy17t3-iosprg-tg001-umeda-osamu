﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;


public class PlayerAttack : MonoBehaviour {

    private bool attacking = false;

    private float attackTimer = 0;
    private float attackCD = 0.3f;

    public Collider2D attackTrigger;

    public GameObject bombDestroyerTrigger;

    private Animator anim;

    void Awake()
    {
        anim = gameObject.GetComponent<Animator>();
        attackTrigger.enabled = false;
    }
    // Use this for initialization
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        Attack();
    }

    void Attack()
    {
        if (CrossPlatformInputManager.GetButtonDown("Fire1") && !attacking)
        {
            attacking = true;
            attackTimer = attackCD;

            bombDestroyerTrigger.SetActive(true);

            attackTrigger.enabled = true;

        }
        if (attacking)
        {
            if (attackTimer > 0)
            {
                attackTimer -= Time.deltaTime;
            }
            else
            {
                attacking = false;
                attackTrigger.enabled = false;
                bombDestroyerTrigger.SetActive(false);
            }
        }
        anim.SetBool("Attacking", attacking);
    }
}
