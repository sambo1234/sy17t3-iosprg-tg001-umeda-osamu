﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPosition : MonoBehaviour {

    public string playerTag = "Player";
    internal Transform playerObject;

    void Start () {
        playerObject = GameObject.FindGameObjectWithTag(playerTag).transform;
        
        playerObject.transform.position = this.transform.position;
            
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
