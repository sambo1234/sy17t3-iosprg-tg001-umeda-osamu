﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAttackTrigger : MonoBehaviour {
    public string playerTag = "Player";
    internal GameObject playerObject;

    public int dmg = 10;
    // Use this for initialization
    void Start () {
        playerObject = GameObject.FindGameObjectWithTag(playerTag).gameObject;
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.isTrigger != true && col.CompareTag("Player"))
        {
            if (!playerObject.GetComponent<PlayerHealth>().invincible)
            {
                col.SendMessageUpwards("TakeDamage", dmg = 10);
            }
            if(playerObject.GetComponent<PlayerHealth>().invincible)
            {
                
            }
            //startBlinking = true;
        }
    }
    // Update is called once per frame
    void Update () {
		
	}
}
