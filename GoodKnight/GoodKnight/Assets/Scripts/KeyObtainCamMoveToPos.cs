﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyObtainCamMoveToPos : MonoBehaviour {

    public GameObject player;
    public string doorTag = "Door";
    public string targetTag = "Target";
    private float speed = 5f;

    internal Transform doorObject;

    private Vector2 targetPosition = Vector2.zero;
    private bool hasStartedMoving = false;
	// Use this for initialization
	void Start () {
        doorObject = GameObject.FindGameObjectWithTag(targetTag).transform;
    }
	
	// Update is called once per frame
	void Update () {
		if(player.GetComponent<Getkey>().keyObtained && !hasStartedMoving)
        {
            hasStartedMoving = true;
            StartCoroutine(TransitionCameraPosition());
            StartCoroutine(DestroyDoor());
        }
        if(hasStartedMoving)
        {
            MoveToTarget();
        }
	}

    private void MoveToTarget()
    {
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(doorObject.position.x, doorObject.position.y, transform.position.z), speed * Time.deltaTime);
    }

    IEnumerator TransitionCameraPosition()
    {
        yield return new WaitUntil(() => Vector2.Distance(transform.position, doorObject.position) < 1f);
        yield return new WaitForSeconds(3);

        Vector3 initialPosition = new Vector3(player.transform.position.x, player.transform.position.y, -13.87f);
        transform.position = initialPosition;

        this.enabled = false;
    }

    IEnumerator DestroyDoor()
    {
        yield return new WaitForSeconds(3f);
        GameObject.FindGameObjectWithTag(doorTag).gameObject.SetActive(false);
    }
}
