﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CallMainMenu : MonoBehaviour {

    public void onClick()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
