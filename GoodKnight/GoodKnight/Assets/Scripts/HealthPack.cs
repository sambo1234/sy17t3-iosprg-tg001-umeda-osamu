﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : MonoBehaviour {
    public string playerTag = "Player";
    internal Transform playerObject;

    //private Animator anim;

    //public GameObject explosion;

    //public GameObject explosion;

    public int healVal = 10;
    void Start()
    {
        //anim = gameObject.GetComponent<Animator>();
        playerObject = GameObject.FindGameObjectWithTag(playerTag).transform;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            HealPlayer();
            //GameObject explode = Instantiate(explosion, transform.position, Quaternion.identity);
            Destroy(gameObject);
            //Destroy(explode, 1);
        }
    }

    public void HealPlayer()
    {
        playerObject.SendMessageUpwards("Heal", healVal);
    }
}
