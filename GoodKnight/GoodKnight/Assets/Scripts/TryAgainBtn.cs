﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TryAgainBtn : MonoBehaviour {

    public void onClick()
    {
        SceneManager.LoadScene("GoodKnight");
    }
}
