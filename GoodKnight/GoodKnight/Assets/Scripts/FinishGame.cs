﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishGame : MonoBehaviour {
    public GameObject boss2;
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		if(boss2.GetComponent<EnemyStatus>().isDead)
        {
            SceneManager.LoadScene("GameEnd");
        }
	}
}
