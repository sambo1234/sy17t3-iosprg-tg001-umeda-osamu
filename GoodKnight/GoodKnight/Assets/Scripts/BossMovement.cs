﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMovement : MonoBehaviour {
    public string playerTag = "Player";
    internal Transform playerObject;

    [SerializeField]
    private float moveSpeed = 5f;

    public Animator anim;
    private bool moving = false;
    //[SerializeField]
    //private int damage = 10;

    Rigidbody2D rb;

    Vector3 localScale;

    float dirX;

    bool facingRight = false;
	// Use this for initialization
	void Start () {
        anim = gameObject.GetComponent<Animator>();
        playerObject = GameObject.FindGameObjectWithTag(playerTag).transform;
        localScale = transform.localScale;
        rb = GetComponent<Rigidbody2D>();
        dirX = -1.0f;
        moving = true;
        anim.SetBool("Moving", moving);
	}
	
	// Update is called once per frame
	void Update () {
		
       
	}

    void FixedUpdate()
    {
        rb.velocity = new Vector2(dirX * moveSpeed, rb.velocity.y);
    }

    void LateUpdate()
    {
        CheckWhereToFace();
    }

    void CheckWhereToFace()
    {
        if(dirX > 0)
            facingRight = false;
        
        else if(dirX < 0)
            facingRight = true;
        

        if(((facingRight) && (localScale.x < 0)) || ((!facingRight) && (localScale.x > 0)))
            localScale.x *= -1;

        transform.localScale = localScale;
            
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        switch(collision.gameObject.tag)
        {
            case "LeftWall":
                dirX = 1f;
                break;

            case "RightWall":
                dirX = -1f;
                break;
        }
    }
}
