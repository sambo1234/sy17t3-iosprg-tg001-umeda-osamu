﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBombDamage : MonoBehaviour {
    public string playerTag = "Player";
    internal Transform playerObject;

    private List<GameObject> spawned = new List<GameObject>();
    private List<GameObject> pooledObjects = new List<GameObject>();

    //public GameObject explosion;

    public int damage = 5;
    void Start()
    {
        playerObject = GameObject.FindGameObjectWithTag(playerTag).transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (!playerObject.GetComponent<PlayerHealth>().invincible)
            {
                DamagePlayer();
            }
            if (playerObject.GetComponent<PlayerHealth>().invincible)
            {

            }
            GameObject explosion = ExplosionObjectPooler.sharedInstance.GetPooledObject();
            if (explosion != null)
            {
                explosion.transform.position = transform.position;
                explosion.SetActive(true);
            }
            //StartCoroutine(destroyExplosionObject(explosion));
            gameObject.SetActive(false);
            
        }
        //if (collision.gameObject.tag == "MeleeAtkTrigger")
        //{
        //    GameObject explosion = ExplosionObjectPooler.sharedInstance.GetPooledObject();
        //    if (explosion != null)
        //    {
        //        explosion.transform.position = transform.position;
        //        explosion.SetActive(true);
        //    }
        //    gameObject.SetActive(false);
        //}
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "MeleeAtkTrigger")
        {
            GameObject explosion = ExplosionObjectPooler.sharedInstance.GetPooledObject();
            if (explosion != null)
            {
                explosion.transform.position = transform.position;
                explosion.SetActive(true);
            }
            gameObject.SetActive(false);
        }

    }

    //IEnumerator destroyExplosionObject(GameObject explodeObject)
    //{
    //    Color tmp = gameObject.GetComponent<SpriteRenderer>().color;
    //    tmp.a = 0f;
    //    gameObject.GetComponent<SpriteRenderer>().color = tmp;
    //    yield return new WaitForSeconds(0.5f);
    //    explodeObject.SetActive(false);
    //    yield return new WaitForSeconds(0.5f);
    //    gameObject.SetActive(false);
    //}
    public void DamagePlayer()
    {
        playerObject.SendMessageUpwards("TakeDamage", damage);
    }
}
