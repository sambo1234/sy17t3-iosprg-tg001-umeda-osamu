﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossJumpAttack : MonoBehaviour {
    public string playerTag = "Player";
    internal Transform playerObject;

    public BossMovement bossMovement;

    public GameObject boss1;

    Vector3 localScale;

    //float dirX;

    public GameObject trigger;

    public GameObject explosion;

    //Rigidbody2D rb;
    public bool faceRight = false;

    //public GameObject recGO;

    [SerializeField]
    private float jumpForce;

    [SerializeField]
    private Transform[] groundPoints;

    [SerializeField]
    private float groundRadius;

    [SerializeField]
    private LayerMask whatIsGround;

    public bool isGrounded;

    private bool jump;

    public float time = 0;
    // Use this for initialization
    void Start () {
        playerObject = GameObject.FindGameObjectWithTag(playerTag).transform;
        /*localScale = *//*transform.localScale;*/
        //rb = GetComponent<Rigidbody2D>();
        //dirX = -1.0f;
        explosion.SetActive(false);
        bossMovement = boss1.GetComponent<BossMovement>();
    }

    void Update()
    {
        
        this.transform.position = boss1.transform.position; // + new Vector3(-5,0,0);
        //if(isGrounded)
        //{
        //    explosion.SetActive(false);
        //    time++;
        //    if(time < 50.0f)
        //    {
        //        explosion.SetActive(true);
        //        explosion.transform.position = trigger.transform.position;
        //    }
        //    if(time > 60.0f)
        //    {
        //        trigger.transform.position = Vector3.zero;
        //        explosion.SetActive(false);
        //    }
        //}

        //if(!isGrounded)
        //{
        //    time = 0;
        //    explosion.SetActive(false);
        //}
    }

    void FixedUpdate()
    {
        isGrounded = IsGrounded();

        jump = false;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        switch (col.tag)
        {
            case "Player":
                jump = true;
                if (isGrounded && jump)
                {
                    isGrounded = false;
                    StartCoroutine(BossJump());
                }
                break;

            case "LeftWall":
                trigger.transform.localScale = new Vector3(-1, 0, 0);
                faceRight = true;
                break;

            case "RightWall":
                trigger.transform.localScale = new Vector3(1, 0, 0);
                faceRight = false;
                break;
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Ground")
        {
            explosion.SetActive(false);
        }
    }

    //void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.gameObject.tag == "Ground")
    //        explosion.SetActive(true);
    //        explosion.transform.position = trigger.transform.position;
    //}

    //void OnCollisionStay2D(Collision2D collision)
    //{
    //    //if (collision.gameObject.tag == "Ground")
    //    //    explosion.SetActive(false);
    //}

    private void AoEDamage()
    {
        //yield return new WaitForSeconds(2);
        //explosion.SetActive(true);
    }

    private bool IsGrounded()
    {
        if (boss1.GetComponent<Rigidbody2D>().velocity.y <= 0)
        {
            foreach (Transform point in groundPoints)
            {
                Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position, groundRadius, whatIsGround);

                for (int i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i].gameObject != gameObject)
                    {
                        return true;

                    }
                }
            }
        }
        return false;
    }

    IEnumerator BossJump()
    {
        
        boss1.GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpForce);
        yield return new WaitForSeconds(1);
        bossMovement.enabled = false;
        explosion.SetActive(true);
        explosion.transform.position = new Vector3(trigger.transform.position.x, -2, 0);
        StartCoroutine(StopExplosion());
        yield return new WaitForSeconds(1);
        bossMovement.enabled = true;
    }

    IEnumerator StopExplosion()
    {
        yield return new WaitForSeconds(2);
        explosion.SetActive(false);
    }
}
