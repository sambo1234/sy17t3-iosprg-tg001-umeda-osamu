﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EnemyStatus : MonoBehaviour {
    public float spriteBlinkingTimer = 0.0f;
    public float spriteBlinkingMiniDuration = 0.1f;
    public float spriteBlinkingTotalTimer = 0.0f;
    public float spriteBlinkingTotalDuration = 1.0f;
    public bool startBlinking = false;

    [SerializeField]
    private float fillAmount;

    [SerializeField]
    private Image content;

    public float currHealth;
    public float maxHealth;
    public float percentHealth = 100;

    public BossAttack bossAttack;
    public GameObject jumpAttack;

    public GameObject boss1;
    public GameObject shootingObject;

    public GameObject canvas;
    public GameObject healthBar;
    public GameObject bossJumpAttackTriggerObject;

    public bool isDead = false;

	// Use this for initialization
	void Start () {
        bossAttack = boss1.GetComponent<BossAttack>();
        currHealth = maxHealth;
        canvas.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        HandleBar();

        if (percentHealth > 70)
        {
            shootingObject.SetActive(false);
            jumpAttack.SetActive(false);
        }

        if(percentHealth < 70 && percentHealth > 30)
        {
            shootingObject.SetActive(true);
        }
        if(percentHealth < 30)
        {
            jumpAttack.SetActive(true);
        }

		if(currHealth <= 0)
        {
            Destroy(gameObject);
            Destroy(bossJumpAttackTriggerObject);
            canvas.SetActive(true);
            healthBar.SetActive(false);
            isDead = true;
        }


        if (startBlinking == true)
        {
            SpriteBlinkingEffect();
        }
    }

    public void Damage(int damage)
    {
        currHealth -= damage;
        
        percentHealth = (100 / maxHealth) * currHealth;
        startBlinking = true;
    }

    private void HandleBar()
    {
        content.fillAmount = Map(currHealth, maxHealth, fillAmount);
    }

    private float Map(float value, float inMax, float outMax)
    {
        return (value) * (outMax) / (inMax);
    }

    private void SpriteBlinkingEffect()
    {
        spriteBlinkingTotalTimer += Time.deltaTime;
        if (spriteBlinkingTotalTimer >= spriteBlinkingTotalDuration)
        {
            startBlinking = false;
            spriteBlinkingTotalTimer = 0.0f;
            this.gameObject.GetComponent<SpriteRenderer>().enabled = true;   // according to 
                                                                            //your sprite
            return;
        }

        spriteBlinkingTimer += Time.deltaTime;
        if (spriteBlinkingTimer >= spriteBlinkingMiniDuration)
        {
            spriteBlinkingTimer = 0.0f;
            if (this.gameObject.GetComponent<SpriteRenderer>().enabled == true)
            {
                this.gameObject.GetComponent<SpriteRenderer>().enabled = false;  //make changes
            }
            else
            {
                this.gameObject.GetComponent<SpriteRenderer>().enabled = true;   //make changes
            }
        }
    }
}
