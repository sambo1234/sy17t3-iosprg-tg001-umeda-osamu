﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionDisableOnCollision : MonoBehaviour {

    void Update()
    {
        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(delayedDisable());
        }
    }

    IEnumerator delayedDisable()
    {
        yield return new WaitForSeconds(1);
        gameObject.SetActive(false);
    }
}
